import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:relative_time/relative_time.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './lib/components/timeline/home_timeline.dart';
import './lib/components/timeline/local_timeline.dart';
import './lib/components/timeline/global_timeline.dart';

final getIt = GetIt.instance;

class Andkey extends StatefulWidget {
  const Andkey({super.key});

  @override
  State<Andkey> createState() => _AndkeyState();
}

class _AndkeyState extends State<Andkey> {
  late String _theme;

  @override
  void initState() {
    super.initState();

    _theme = getIt<SharedPreferences>().getString("theme") ?? "light";
    if (_theme != "light" && _theme != "dark" && _theme != "black") {
      _theme = "light";
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Andkey',
      localizationsDelegates: const [
        RelativeTimeLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('en'),
      ],
      theme: _themeData(),
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Home'),
          ),
          bottomNavigationBar: Container(
            color: _theme != "light" ? Colors.black : Colors.white,
            child: TabBar(
              labelColor: _theme != "light" ? Colors.white : Colors.black,
              indicatorColor: _theme != "light" ? Colors.white : Colors.black,
              tabs: const [
                Tab(icon: Icon(Icons.home), text: 'Home'),
                Tab(icon: Icon(Icons.people), text: 'Local'),
                Tab(icon: Icon(Icons.public), text: 'Global'),
              ],
            ),
          ),
          body: const TabBarView(
            children: [
              HomeTimeline(),
              LocalTimeline(),
              GlobalTimeline(),
            ],
          ),
        ),
      ),
    );
  }

  ThemeData _themeData() {
    switch (_theme) {
      case "dark":
        return darkTheme();
      case "black":
        return blackTheme();
    }

    return lightTheme();
  }
}

ThemeData lightTheme() {
  final base = ThemeData.light();
  return base.copyWith(
    appBarTheme: const AppBarTheme(
      backgroundColor: Colors.lightGreen,
    ),
  );
}

ThemeData darkTheme() {
  final base = ThemeData.dark();
  return base.copyWith(
    appBarTheme: const AppBarTheme(
      backgroundColor: Colors.green,
    ),
  );
}

ThemeData blackTheme() {
  final base = ThemeData.dark();
  return base.copyWith(
      backgroundColor: Colors.black,
      colorScheme: const ColorScheme.dark().copyWith(
        background: Colors.black,
      ));
}
