// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'misskey_client.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotesTimelineRequest _$NotesTimelineRequestFromJson(
        Map<String, dynamic> json) =>
    NotesTimelineRequest(
      json['i'] as String,
      json['sinceId'] as String?,
      json['untilId'] as String?,
      json['limit'] as int,
      json['withFiles'] as bool,
      includeMyRenotes: json['includeMyRenotes'] as bool?,
      includeRenotedMyNotes: json['includeRenotedMyNotes'] as bool?,
      includeLocalRenotes: json['includeLocalRenotes'] as bool?,
    );

Map<String, dynamic> _$NotesTimelineRequestToJson(
    NotesTimelineRequest instance) {
  final val = <String, dynamic>{
    'i': instance.token,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('sinceId', instance.sinceId);
  writeNotNull('untilId', instance.untilId);
  val['limit'] = instance.limit;
  val['withFiles'] = instance.withFiles;
  writeNotNull('includeMyRenotes', instance.includeMyRenotes);
  writeNotNull('includeRenotedMyNotes', instance.includeRenotedMyNotes);
  writeNotNull('includeLocalRenotes', instance.includeLocalRenotes);
  return val;
}

APIError _$APIErrorFromJson(Map<String, dynamic> json) => APIError(
      InnerError.fromJson(json['error'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$APIErrorToJson(APIError instance) => <String, dynamic>{
      'error': instance.error,
    };

InnerError _$InnerErrorFromJson(Map<String, dynamic> json) => InnerError(
      json['message'] as String,
      json['code'] as String,
      json['id'] as String,
    );

Map<String, dynamic> _$InnerErrorToJson(InnerError instance) =>
    <String, dynamic>{
      'message': instance.message,
      'code': instance.code,
      'id': instance.id,
    };
