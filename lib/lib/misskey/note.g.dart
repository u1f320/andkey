// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'note.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Note _$NoteFromJson(Map<String, dynamic> json) => Note(
      json['id'] as String,
      DateTime.parse(json['createdAt'] as String),
      json['text'] as String?,
      json['cw'] as String?,
      (json['emojis'] as List<dynamic>?)
          ?.map((e) => Emoji.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['userId'] as String,
      User.fromJson(json['user'] as Map<String, dynamic>),
      json['replyId'] as String?,
      json['renoteId'] as String?,
      json['reply'] == null
          ? null
          : Note.fromJson(json['reply'] as Map<String, dynamic>),
      json['renote'] == null
          ? null
          : Note.fromJson(json['renote'] as Map<String, dynamic>),
      json['visibility'] as String,
      json['localOnly'] as bool?,
      (json['mentions'] as List<dynamic>?)?.map((e) => e as String).toList(),
      (json['visibleUserIds'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      (json['fileIds'] as List<dynamic>?)?.map((e) => e as String).toList(),
      json['files'] as List<dynamic>?,
      (json['tags'] as List<dynamic>?)?.map((e) => e as String).toList(),
      json['poll'],
      json['channelId'] as String?,
      json['channel'],
      json['reactions'] as Map<String, dynamic>?,
      json['renoteCount'] as int,
      json['repliesCount'] as int,
      json['uri'] as String?,
      json['url'] as String?,
    );

Map<String, dynamic> _$NoteToJson(Note instance) => <String, dynamic>{
      'id': instance.id,
      'createdAt': instance.createdAt.toIso8601String(),
      'text': instance.text,
      'cw': instance.cw,
      'emojis': instance.emojis,
      'poll': instance.poll,
      'userId': instance.userId,
      'user': instance.user,
      'replyId': instance.replyId,
      'renoteId': instance.renoteId,
      'reply': instance.reply,
      'renote': instance.renote,
      'visibility': instance.visibility,
      'localOnly': instance.localOnly,
      'mentions': instance.mentions,
      'visibleUserIds': instance.visibleUserIds,
      'reactions': instance.reactions,
      'renoteCount': instance.renoteCount,
      'repliesCount': instance.repliesCount,
      'uri': instance.uri,
      'url': instance.url,
      'fileIds': instance.fileIds,
      'files': instance.files,
      'tags': instance.tags,
      'channelId': instance.channelId,
      'channel': instance.channel,
    };
