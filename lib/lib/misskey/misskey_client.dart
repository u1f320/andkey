import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:json_annotation/json_annotation.dart';

import './body.dart';
import './note.dart';

part 'misskey_client.g.dart';

class MisskeyClient {
  late final String _token;
  late final String baseUrl;
  final http.Client _client = http.Client();

  MisskeyClient(this._token, this.baseUrl);

  Future<http.Response> _send<T extends BaseRequest>(Uri uri, T reqBody) async {
    final resp = await _client.post(
      uri,
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(reqBody.toJson()),
    );

    if (resp.statusCode < 200 || resp.statusCode > 399) {
      final decodedBody = jsonDecode(resp.body);
      throw APIError.fromJson(decodedBody);
    }

    return resp;
  }

  Uri _notesTimelineUri() => Uri.https(baseUrl, '/api/notes/timeline');

  Future<List<Note>> notesTimeline(
      {String? sinceId,
      String? untilId,
      int limit = 100,
      bool includeMyRenotes = true,
      bool includeRenotedMyNotes = true,
      bool includeLocalRenotes = true,
      bool withFiles = false}) async {
    final resp = await _send(
        _notesTimelineUri(),
        NotesTimelineRequest(
            _token,
            sinceId,
            untilId,
            limit,
            includeMyRenotes: includeMyRenotes,
            includeRenotedMyNotes: includeRenotedMyNotes,
            includeLocalRenotes: includeLocalRenotes,
            withFiles));

    final body = jsonDecode(resp.body);
    return (body as List<dynamic>)
        .map((e) => Note.fromJson(e as Map<String, dynamic>))
        .toList();
  }

  Uri _notesLocalTimelineUri() =>
      Uri.https(baseUrl, '/api/notes/local-timeline');

  Future<List<Note>> notesLocalTimeline(
      {String? sinceId,
      String? untilId,
      int limit = 100,
      bool withFiles = false}) async {
    final resp = await _send(_notesLocalTimelineUri(),
        NotesTimelineRequest(_token, sinceId, untilId, limit, withFiles));

    final body = jsonDecode(resp.body);
    return (body as List<dynamic>)
        .map((e) => Note.fromJson(e as Map<String, dynamic>))
        .toList();
  }

  Uri _notesGlobalTimelineUri() =>
      Uri.https(baseUrl, '/api/notes/global-timeline');

  Future<List<Note>> notesGlobalTimeline(
      {String? sinceId,
      String? untilId,
      int limit = 100,
      bool withFiles = false}) async {
    final resp = await _send(_notesGlobalTimelineUri(),
        NotesTimelineRequest(_token, sinceId, untilId, limit, withFiles));

    final body = jsonDecode(resp.body);
    return (body as List<dynamic>)
        .map((e) => Note.fromJson(e as Map<String, dynamic>))
        .toList();
  }
}

@JsonSerializable()
class NotesTimelineRequest extends BaseRequest {
  @JsonKey(includeIfNull: false)
  String? sinceId;

  @JsonKey(includeIfNull: false)
  String? untilId;

  int limit;
  bool withFiles;

  @JsonKey(includeIfNull: false)
  bool? includeMyRenotes;
  @JsonKey(includeIfNull: false)
  bool? includeRenotedMyNotes;
  @JsonKey(includeIfNull: false)
  bool? includeLocalRenotes;

  NotesTimelineRequest(
    String token,
    this.sinceId,
    this.untilId,
    this.limit,
    this.withFiles, {
    this.includeMyRenotes,
    this.includeRenotedMyNotes,
    this.includeLocalRenotes,
  }) : super(token);

  factory NotesTimelineRequest.fromJson(Map<String, dynamic> json) =>
      _$NotesTimelineRequestFromJson(json);
  @override
  Map<String, dynamic> toJson() => _$NotesTimelineRequestToJson(this);
}

@JsonSerializable()
class APIError {
  InnerError error;

  APIError(this.error);

  factory APIError.fromJson(Map<String, dynamic> json) =>
      _$APIErrorFromJson(json);
  Map<String, dynamic> toJson() => _$APIErrorToJson(this);
}

@JsonSerializable()
class InnerError implements RequestResponseBody {
  String message;
  String code;
  String id;

  InnerError(this.message, this.code, this.id);

  factory InnerError.fromJson(Map<String, dynamic> json) =>
      _$InnerErrorFromJson(json);
  @override
  Map<String, dynamic> toJson() => _$InnerErrorToJson(this);
}
