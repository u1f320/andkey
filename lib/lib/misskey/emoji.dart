import 'package:json_annotation/json_annotation.dart';

part 'emoji.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class Emoji {
  String name;
  String? url;

  Emoji(this.name, this.url);

  factory Emoji.fromJson(Map<String, dynamic> json) => _$EmojiFromJson(json);
  Map<String, dynamic> toJson() => _$EmojiToJson(this);

  @override
  String toString() => 'Emoji<$name>: $url';
}
