import 'package:json_annotation/json_annotation.dart';

import './user.dart';
import './emoji.dart';

part 'note.g.dart';

@JsonSerializable()
class Note {
  final String id;
  final DateTime createdAt;

  final String? text;
  final String? cw;
  final List<Emoji>? emojis;
  final dynamic poll;

  final String userId;
  final User user;

  String? replyId;
  String? renoteId;
  Note? reply;
  Note? renote;

  String visibility;
  bool? localOnly;

  List<String>? mentions;
  List<String>? visibleUserIds;
  Map<String, dynamic>? reactions;
  int renoteCount;
  int repliesCount;

  String? uri;
  String? url;

  List<String>? fileIds;
  List<dynamic>? files;

  List<String>? tags;

  String? channelId;
  dynamic channel;

  Note(
      this.id,
      this.createdAt,
      this.text,
      this.cw,
      this.emojis,
      this.userId,
      this.user,
      this.replyId,
      this.renoteId,
      this.reply,
      this.renote,
      this.visibility,
      this.localOnly,
      this.mentions,
      this.visibleUserIds,
      this.fileIds,
      this.files,
      this.tags,
      this.poll,
      this.channelId,
      this.channel,
      this.reactions,
      this.renoteCount,
      this.repliesCount,
      this.uri,
      this.url);

  factory Note.fromJson(Map<String, dynamic> json) => _$NoteFromJson(json);
  Map<String, dynamic> toJson() => _$NoteToJson(this);

  @override
  String toString() => 'Note<$id>: "$text"';
}
