import 'package:json_annotation/json_annotation.dart';

import './emoji.dart';

part 'user.g.dart';

enum OnlineStatus { unknown, online, active, offline }

@JsonSerializable()
class User {
  final String id;
  final String? name;
  final String username;
  final String? host;
  final String? avatarUrl;
  final bool? isAdmin;
  final bool? isModerator;
  final bool? isBot;
  final bool? isCat;

  final List<Emoji>? emojis;
  final String onlineStatus;

  User(
      this.id,
      this.name,
      this.username,
      this.host,
      this.avatarUrl,
      this.isAdmin,
      this.isModerator,
      this.isBot,
      this.isCat,
      this.emojis,
      this.onlineStatus);

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);

  @override
  String toString() => username;
}
