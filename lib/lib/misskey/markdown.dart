import 'package:markdown/markdown.dart';

import './note.dart';

String noteToHtml(Note note) {
  final syntaxes = <InlineSyntax>[
    _EmojiSyntax(note),
    _MentionSyntax(note),
    _LineBreakSyntax(),
    AutolinkExtensionSyntax(),
  ];

  return markdownToHtml(note.text!, inlineSyntaxes: syntaxes);
}

class _EmojiSyntax extends InlineSyntax {
  final Note note;

  _EmojiSyntax(this.note) : super(':([a-z0-9_-]+):');

  @override
  bool onMatch(InlineParser parser, Match match) {
    if (note.emojis == null) {
      parser.advanceBy(1);
      return false;
    }

    final emojis = note.emojis!;

    final name = match[1]!;

    final index = emojis.indexWhere((element) => element.name == name);
    if (index == -1 || emojis[index].url == null) {
      parser.advanceBy(1);
      return false;
    }

    // create custom emojo element (this is later replaced with a custom widget)
    final elem = Element.empty('emojo');
    elem.attributes['name'] = name;
    elem.attributes['src'] = emojis[index].url!;

    parser.addNode(elem);
    return true;
  }
}

class _MentionSyntax extends InlineSyntax {
  final Note note;

  _MentionSyntax(this.note) : super(r'@([a-z0-9_-]+)(@([a-z0-9+\.-]+))?');

  @override
  bool onMatch(InlineParser parser, Match match) {
    final fullUser = match[0]!;
    final username = match[1]!;
    final host = match[3];

    // create custom mention element
    final elem = Element.text('mention', fullUser);
    elem.attributes['user'] = username;
    elem.attributes['full'] = fullUser;
    if (host != null) {
      elem.attributes['host'] = host;
    }

    parser.addNode(elem);
    return true;
  }
}

class _LineBreakSyntax extends InlineSyntax {
  _LineBreakSyntax() : super(r'\n');

  /// Create a void <br> element.
  @override
  bool onMatch(InlineParser parser, Match match) {
    parser.addNode(Element.empty('br'));
    return true;
  }
}
