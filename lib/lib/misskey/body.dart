import 'package:json_annotation/json_annotation.dart';

part 'body.g.dart';

abstract class RequestResponseBody {
  factory RequestResponseBody.fromJson(Map<String, dynamic> json) =>
      RequestResponseBody.fromJson({});
  Map<String, dynamic> toJson();
}

@JsonSerializable()
class BaseRequest implements RequestResponseBody {
  @JsonKey(name: 'i', includeIfNull: false)
  String token;

  BaseRequest(this.token);

  factory BaseRequest.fromJson(Map<String, dynamic> json) =>
      _$BaseRequestFromJson(json);
  @override
  Map<String, dynamic> toJson() => _$BaseRequestToJson(this);
}
