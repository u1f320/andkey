import 'package:andkey/lib/misskey/misskey_client.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:get_it/get_it.dart';

import './timeline.dart';
import '../../misskey/note.dart';

final getIt = GetIt.instance;

class HomeTimeline extends ConsumerWidget {
  const HomeTimeline({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final scrollController = ScrollController();
    final notes = ref.watch(homeTimelineNotes);

    // fetch initial notes in the background
    if (notes.isEmpty) {
      final client = getIt<MisskeyClient>();
      try {
        ref.read(homeTimelineNotes.notifier).fetchNotes(client.notesTimeline());
      } catch (e) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Error fetching notes: $e')),
        );
      }
    }

    scrollController.addListener(() async {
      if (scrollController.position.pixels !=
          scrollController.position.maxScrollExtent) {
        return;
      }

      try {
        final client = getIt<MisskeyClient>();

        if (notes.isNotEmpty) {
          await ref
              .read(homeTimelineNotes.notifier)
              .fetchNotes(client.notesTimeline(untilId: notes.last.id));
        } else {
          await ref
              .read(homeTimelineNotes.notifier)
              .fetchNotes(client.notesTimeline());
        }
      } catch (e) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Error fetching notes: $e')),
        );
      }
    });

    return RefreshIndicator(
      onRefresh: () async {
        try {
          final client = getIt<MisskeyClient>();

          await ref.read(homeTimelineNotes.notifier).fetchNotes(
              client.notesTimeline(sinceId: notes.first.id),
              after: false);
        } catch (e) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text('Error refreshing notes: $e')),
          );
        }
      },
      child: notes.isNotEmpty
          ? Timeline(
              notes: notes,
              controller: scrollController,
            )
          : Center(
              child: Column(
              children: const [
                CircularProgressIndicator(),
                Text(
                  'Loading notes',
                  style: TextStyle(fontSize: 20),
                ),
              ],
            )),
    );
  }
}

final homeTimelineNotes =
    StateNotifierProvider<TimelineState, List<Note>>((ref) => TimelineState());
