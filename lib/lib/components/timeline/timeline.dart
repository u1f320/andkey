import 'package:andkey/lib/components/note.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../misskey/note.dart';

class Timeline extends StatelessWidget {
  final List<Note> notes;
  final ScrollController controller;

  const Timeline({super.key, required this.notes, required this.controller});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: (context, index) {
        return NoteListWidget(note: notes[index]);
      },
      separatorBuilder: (_, __) => const Divider(height: 1, color: Colors.grey),
      itemCount: notes.length,
      controller: controller,
      physics: const AlwaysScrollableScrollPhysics(),
    );
  }
}

class TimelineState extends StateNotifier<List<Note>> {
  TimelineState() : super(List<Note>.empty(growable: true));
  bool _isLoading = false;

  void addNotes(Iterable<Note> notes, {bool after = true}) {
    if (after) {
      state = [...state, ...notes];
    } else {
      state = [...notes, ...state];
    }
  }

  void removeNote(String noteId) {
    state = [
      for (final note in state)
        if (note.id != noteId) note,
    ];
  }

  void updateNote(Note updatedNote) {
    state = [
      for (final note in state)
        if (note.id != updatedNote.id) note else updatedNote,
    ];
  }

  Future<void> fetchNotes(Future<List<Note>> future,
      {bool after = true}) async {
    if (_isLoading) return;

    _isLoading = true;

    try {
      final notes = await future;
      addNotes(notes, after: after);
    } finally {
      _isLoading = false;
    }
  }
}
