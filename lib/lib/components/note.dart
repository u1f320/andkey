import 'package:andkey/lib/components/note_body.dart';
import 'package:andkey/lib/misskey/markdown.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:relative_time/relative_time.dart';
import 'package:flutter_html/flutter_html.dart';

import '../misskey/note.dart';

class FullNoteWidget extends StatelessWidget {
  final Note note;

  const FullNoteWidget({super.key, required this.note});

  @override
  Widget build(BuildContext context) {
    final username = note.user.host != null
        ? TextSpan(text: ' @${note.user.username}', children: [
            TextSpan(
                text: '@${note.user.host} ',
                style: const TextStyle(color: Colors.grey))
          ])
        : TextSpan(text: ' @${note.user.username} ');

    final text = note.text != null ? noteToHtml(note) : null;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: CachedNetworkImage(
                  width: 50,
                  height: 50,
                  imageUrl: note.user.avatarUrl ?? "",
                  placeholder: (context, url) =>
                      const CircularProgressIndicator(),
                  errorWidget: (context, url, _) => const Icon(Icons.error),
                ),
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 5, right: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        note.user.name ?? note.user.username,
                        style: const TextStyle(
                            fontWeight: FontWeight.w700, fontSize: 16),
                      ),
                      Text.rich(username),
                    ],
                  )),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10),
          child: NoteBody(note: note),
        ),
        const Divider(height: 1, color: Colors.grey),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Text(
            '${note.createdAt} (${RelativeTime(context).format(note.createdAt)})',
            style: const TextStyle(color: Colors.grey),
          ),
        )
      ],
    );
  }
}

class NoteListWidget extends StatelessWidget {
  final Function()? onTap;
  final Note note;

  const NoteListWidget({super.key, required this.note, this.onTap});

  @override
  Widget build(BuildContext context) {
    final username = note.user.host != null
        ? TextSpan(text: '@${note.user.username}', children: [
            TextSpan(
                text: '@${note.user.host}',
                style: const TextStyle(color: Colors.grey))
          ])
        : TextSpan(text: '@${note.user.username}');

    return Padding(
      padding: const EdgeInsets.all(10),
      child: Container(
        constraints:
            BoxConstraints(maxWidth: MediaQuery.of(context).size.width),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: CachedNetworkImage(
                width: 50,
                height: 50,
                imageUrl: note.user.avatarUrl ?? "",
                placeholder: (context, url) =>
                    const CircularProgressIndicator(),
                errorWidget: (context, url, _) => const Icon(Icons.error),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(left: 5, right: 5),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          note.user.name != null
                              ? '${note.user.name} '
                              : '${note.user.username} ',
                          style: const TextStyle(fontWeight: FontWeight.w700),
                        ),
                        const Spacer(),
                        Text(RelativeTime(context).format(note.createdAt))
                      ],
                    ),
                    Text.rich(
                      username,
                      softWrap: true,
                      style: const TextStyle(
                        overflow: TextOverflow.fade,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: NoteBody(note: note),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class IconButton extends StatelessWidget {
  final IconData icon;
  final Function()? onTap;
  final int? count;

  const IconButton({super.key, required this.icon, this.onTap, this.count});

  @override
  Widget build(BuildContext context) {
    final row = Row(
      children: [
        Icon(
          icon,
          color: Colors.grey,
        ),
      ],
    );
    if (count != null) {
      row.children.add(Text(
        '$count',
        style: const TextStyle(color: Colors.grey, fontWeight: FontWeight.w700),
      ));
    }

    if (onTap != null) {
      return InkWell(
        onTap: onTap,
        child: row,
      );
    }

    return row;
  }
}
