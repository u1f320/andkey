import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../misskey/markdown.dart';
import '../misskey/note.dart';

class NoteBody extends StatefulWidget {
  final Note note;

  const NoteBody({super.key, required this.note});

  @override
  State<NoteBody> createState() => _NoteBodyState();
}

class _NoteBodyState extends State<NoteBody> {
  bool _isCollapsed = false;

  @override
  void initState() {
    _isCollapsed = widget.note.cw != null;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var column = List<Widget>.empty(growable: true);

    if (widget.note.cw != null) {
      column.add(Text(widget.note.cw!));
      column.add(OutlinedButton(
        onPressed: () => setState(() {
          _isCollapsed = !_isCollapsed;
        }),
        child: _isCollapsed
            ? const Text('Show content')
            : const Text('Hide content'),
      ));
    }

    var maybeHidden = List<Widget>.empty(growable: true);

    // only render text if text exists (duh)
    if (widget.note.text != null) {
      maybeHidden.add(HtmlBody(note: widget.note));
    } else {
      maybeHidden.add(const Text("woops no content"));
    }

    // only show note content if not collapsed because of CW
    if (!_isCollapsed) {
      column.addAll(maybeHidden);
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: column,
    );
  }
}

class HtmlBody extends StatelessWidget {
  final Note note;

  const HtmlBody({super.key, required this.note});

  @override
  Widget build(BuildContext context) {
    final htmlBody = noteToHtml(note);

    return Html(
      data: htmlBody,
      style: {
        '.emoji': Style(height: 8, width: 8),
      },
      customRender: {
        'mention': (context, parsedChild) {
          final username = context.tree.element!.attributes['full']!;

          return Text(
            username,
            style: const TextStyle(
                fontWeight: FontWeight.w600, color: Colors.blue),
          );
        },
        'emojo': (context, parsedChild) {
          final name = context.tree.element!.attributes['name']!;
          final url = context.tree.element!.attributes['src']!;

          return CachedNetworkImage(
            imageUrl: url,
            height: 30,
            placeholder: (context, url) => const CircularProgressIndicator(),
            errorWidget: (context, url, _) => Text(':$name:'),
          );
        },
      },
      tagsList: Html.tags..addAll(["mention", "emojo"]),
    );
  }
}
