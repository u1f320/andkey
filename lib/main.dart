import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './lib/misskey/misskey_client.dart';
import './lib/misskey/_token.dart';

import './app.dart';

final getIt = GetIt.instance;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final prefs = await SharedPreferences.getInstance();
  getIt.registerSingleton<SharedPreferences>(prefs);

  // final secure = prefs.getBool("secure") ?? true;
  // final host = prefs.getString("host");
  // final _token = prefs.getString("token");

  getIt.registerSingleton<MisskeyClient>(MisskeyClient(token, host));

  runApp(const ProviderScope(child: Andkey()));
}
